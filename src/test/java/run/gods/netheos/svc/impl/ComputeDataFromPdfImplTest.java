package run.gods.netheos.svc.impl;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import run.gods.netheos.svc.ComputeDataFromPdf;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class ComputeDataFromPdfImplTest {

    @Inject
    ComputeDataFromPdf computeDataFromPdf;

    @Test
    void analyse() {
        computeDataFromPdf.analyse("test");
    }
}