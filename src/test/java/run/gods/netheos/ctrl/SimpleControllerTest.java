package run.gods.netheos.ctrl;

import io.quarkus.test.junit.QuarkusTest;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static groovyjarjarantlr4.v4.runtime.misc.Utils.readFile;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class SimpleControllerTest {

    @Test
    void uploadWrongPdf() throws IOException, URISyntaxException {
        URL resource = getClass().getClassLoader().getResource("test_6.pdf");
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            File file = new File(resource.toURI().getPath());
            String s = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
            given()
                    .when()
                    .body(s)
                    .post("/api/v1/simple")
                    .then()
                    .statusCode(415);

        }
    }
}