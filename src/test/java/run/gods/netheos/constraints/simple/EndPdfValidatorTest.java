package run.gods.netheos.constraints.simple;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EndPdfValidatorTest {

    @Test
    void pdfIsValid() throws IOException, URISyntaxException {
        EndPdfValidator endPdfValidator = new EndPdfValidator();
        URL resource = getClass().getClassLoader().getResource("test_1.pdf");
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            File file = new File(resource.toURI().getPath());
            String s = FileUtils.readFileToString(file, StandardCharsets.US_ASCII);
            assertEquals(true, endPdfValidator.isValid(s, null));
        }

    }

    @Test
    void pdfIsNotValid() throws IOException, URISyntaxException {
        EndPdfValidator endPdfValidator = new EndPdfValidator();
        URL resource = getClass().getClassLoader().getResource("test_6.pdf");
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            File file = new File(resource.toURI().getPath());
            String s = FileUtils.readFileToString(file, StandardCharsets.US_ASCII);
            assertEquals(false, endPdfValidator.isValid(s, null));
        }

    }
}