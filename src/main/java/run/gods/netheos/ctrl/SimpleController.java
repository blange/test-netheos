package run.gods.netheos.ctrl;

import run.gods.netheos.constraints.simple.ValidatePdf;
import run.gods.netheos.svc.ComputeDataFromPdf;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/simple")
public class SimpleController {

    @Inject
    ComputeDataFromPdf computeDataFromPdf;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/pdf")
    public void uploadPdf(@ValidatePdf String file) {
        computeDataFromPdf.analyse(file);
    }
}