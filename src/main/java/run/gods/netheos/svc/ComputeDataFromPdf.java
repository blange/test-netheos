package run.gods.netheos.svc;

public interface ComputeDataFromPdf {
    void analyse(String file);
}
