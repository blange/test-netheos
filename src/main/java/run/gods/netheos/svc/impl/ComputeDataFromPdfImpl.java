package run.gods.netheos.svc.impl;

import io.quarkus.logging.Log;
import run.gods.netheos.svc.ComputeDataFromPdf;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ComputeDataFromPdfImpl implements ComputeDataFromPdf {
    public void analyse(String file) {
        Log.info("Do the job");
    }
}
