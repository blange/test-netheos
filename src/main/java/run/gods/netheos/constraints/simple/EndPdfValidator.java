package run.gods.netheos.constraints.simple;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EndPdfValidator implements ConstraintValidator<ValidatePdf, String> {
    static final String END_OF_FILE_STRING = "\n%%EOF";
    static final String START_OF_FILE_STRING = "%PDF";

    @Override
    public boolean isValid(String pdfInString, ConstraintValidatorContext context) {
        if (CheckIfHeaderOfPdfIsValid(pdfInString)) return false;
        return !CheckIfFooterOfPdfIsValid(pdfInString);
    }

    private boolean CheckIfFooterOfPdfIsValid(String pdfInString) {
        int position = -1;
        int lastEndOfFile = pdfInString.indexOf(END_OF_FILE_STRING);
        while (lastEndOfFile != -1) {
            position = lastEndOfFile;
            lastEndOfFile = pdfInString.indexOf(END_OF_FILE_STRING, lastEndOfFile + 1);
        }
        for (int i = END_OF_FILE_STRING.length() + position; i < pdfInString.length(); i++) {
            //LF en ASCII
            if (pdfInString.charAt(i) != 10) {
                return true;
            }
        }
        return false;
    }

    private boolean CheckIfHeaderOfPdfIsValid(String pdfInString) {
        int pdfStartIsAtTheCorrectPosition = pdfInString.indexOf(START_OF_FILE_STRING);
        return pdfStartIsAtTheCorrectPosition != 0;
    }
}
