package run.gods.netheos.constraints.simple;

import javax.validation.Constraint;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER,
        ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EndPdfValidator.class)
@Documented
public @interface ValidatePdf {
    String message() default "PDF is corrupted";
    Class<?>[] groups() default {};
    Class<?>[] payload() default {};
}
